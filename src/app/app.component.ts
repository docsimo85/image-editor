import {AfterViewInit, Component, OnChanges, ViewChild} from '@angular/core';
import {IOptions, ToastUiImageEditorComponent} from 'ngx-tui-image-editor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnChanges {

  @ViewChild(ToastUiImageEditorComponent) editorComponent: ToastUiImageEditorComponent;
  title = 'ang7image';
  options: IOptions = {
    cssMaxWidth: 1000,
    includeUI: {
      theme: {
        'common.backgroundColor': 'black',
        'header.backgroundColor': 'red',
        // tslint:disable-next-line:max-line-length
        'common.bi.image': 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/240px-Angular_full_color_logo.svg.png',
        'common.bisize.height': '50px',
        'common.bisize.width': '50px',
        'downloadButton.backgroundColor': 'green',
        'downloadButton.border': 'green'

      }
    },
    cssMaxHeight: null,
    selectionStyle: {
      cornerColor: 'red',
      borderColor: 'blue'
    },
    usageStatistics: false
  };

  ngAfterViewInit() {
    console.log(this.editorComponent.editorInstance);
  }

  ngOnChanges(changes: any): void {
    this.editorComponent.addText.subscribe(
      (data) => console.log(data)
    );
  }
}
